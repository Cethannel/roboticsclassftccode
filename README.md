# RoboticsClassFTCCode

## Get code

`git clone https://gitlab.com/Cethannel/roboticsclassftccode.git`

## Pull down the latest version

__MUST PULL BEFORE WORKING ON CODE__

`git pull`

## Send upload your code

This step can be skipped by running upload.sh in the bash terminal with ./upload.sh when in the folder. (The commit message should say what you changed)

__MUST PULL BEFORE WORKING ON CODE__

```
git add .
git commit -m "Your message"
git push
```

# Git for windows (must download)
Download git [here](https://github.com/git-for-windows/git/releases/download/v2.28.0.windows.1/Git-2.28.0-32-bit.exe) 

# Partslist
* [Sensor Bundle](https://www.revrobotics.com/rev-45-1885/)
* [Cable Bundle](https://www.revrobotics.com/rev-45-1901/)
* [Servo Bundle](https://www.revrobotics.com/rev-45-1892/)
* [Mecanum Wheel](https://www.revrobotics.com/rev-45-1655/)
* Belts
    * [105 Tooth](https://www.revrobotics.com/rev-41-1799/)
    * [145 Tooth](https://www.revrobotics.com/rev-41-1801/)
    * [210 Tooth](https://www.revrobotics.com/rev-41-1802/)
    * [270 Tooth](https://www.revrobotics.com/rev-41-1803/)
    * [55 Tooth](https://www.revrobotics.com/rev-41-1797/)
    * [85 Tooth](https://www.revrobotics.com/rev-41-1798/)
